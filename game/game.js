const tela = document.querySelector('canvas');
tela.width = 600;
tela.height = 400;

let pincel = tela.getContext('2d');

let Xraio;
let Yraio;
let raio = 10;

limpar = () => pincel.clearRect(0,0,600,400);
(0, 0, 600, 400);

function criarCirculo(x, y, raio, color){
    pincel.beginPath();
    pincel.fillStyle = color;
    pincel.arc(x, y, raio,0, 2 * Math.PI)
    pincel.fill();
}

function alvo(){
    limpar();
    Xraio = Math.floor(Math.random() * 600);
    Yraio = Math.floor(Math.random() * 400);

    criarCirculo(Xraio,Yraio,raio + 20,'red');
    criarCirculo(Xraio,Yraio,raio + 10,'white');
    criarCirculo(Xraio,Yraio,raio, 'red');
}

function acertou(event){

    let x = event.pageX;
    let y = event.pageY;

    if((x > Xraio - raio) && 
       (x < Xraio + raio) &&
       (y > Yraio - raio) && 
       (y < Yraio + raio)){
           alert('Acertou');
       }
}

setInterval(alvo, 5000);
tela.onclick = acertou;



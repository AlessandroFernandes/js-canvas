let botao = document.querySelector('#enviar');
const tela = document.querySelector('canvas');
tela.width = 600;
tela.height = 400;

let pincel = tela.getContext('2d');

pincel.fillStyle = 'red';
pincel.fillRect(0,0,600,400);

let habilita = false;

tela.onmousemove = (event)=> {
    if(habilita){
        pincel.beginPath();
        pincel.fillStyle = botao.value;
        pincel.arc(event.offsetX, event.offsetY,
                event.shiftKey ? 20 + 20 : 20,
                0, 2 * Math.PI);
        pincel.fill();
    }
}

tela.onmousedown = ()=> habilita = true;
tela.onmouseup = ()=> habilita = false;

let cores = 'blue';

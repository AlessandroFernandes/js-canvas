const tela = document.querySelector('canvas');
tela.width = 600;
tela.height = 400;

let pincel = tela.getContext('2d');
pincel.fillStyle = "lightgray";
pincel.fillRect(0,0,600,400);

let raio = 10;

function desenhaCirculo(x, y, raio, cor){
    pincel.beginPath();
    pincel.fillStyle = cor;
    pincel.arc(x, y,raio,0, 2 * Math.PI);
    pincel.fill();
}

desenhaCirculo(300,200,raio + 20, 'red');
desenhaCirculo(300,200,raio + 10, 'white');
desenhaCirculo(300,200,raio, 'red');

function disparar(event){
    const x = event.offsetX;
    const y = event.offsetY;

  
   if(x > 300 - raio - 20 && x < 300 + raio + 20 && y > 200 -raio -20 && y < 200 + raio +20) {
        console.log('acertou');
    }
}

tela.onclick = disparar;
const tela = document.querySelector('canvas');
tela.width = 100;
tela.height = 100;

let pincel = tela.getContext('2d');

const s2015 = [50,25,20,5];
const s2016 = [65,20,13,2];

const cores = ['blue','green','yellow','red'];

function quadrado(x, y, cor){
    pincel.fillStyle = cor;
    pincel.fillRect(x,y,50,100);
    pincel.strokeStyle = 'black';
    pincel.strokeRect(x,y,50,100);
}
function texto(x, y, texto){
    pincel.font = '15px Georgia';
    pincel.fillStyle = 'black';
    pincel.fillText(texto, x, y);
}

function barra(reg, ano, cor){
    let barra = 100;
    for(let x = 0; x < ano.length; x++){
        quadrado(reg, barra - ano[x], cor[x])
    };
};

barra(0, s2015 , cores);
barra(50, s2016, cores);
texto(10,10,'2015');
texto(60,10,'2016');
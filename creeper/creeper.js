const tela = document.querySelector('canvas');
let pincel = tela.getContext('2d');

tela.width = 600;
tela.height = 400;

pincel.fillStyle = 'darkgreen';
pincel.fillRect(0,0,350,300);

pincel.fillStyle = 'black';
pincel.fillRect(50,50,90,90);
pincel.fillRect(210,50,90,90);
pincel.fillRect(140,140,70,100);
pincel.fillRect(100,190,40,110);
pincel.fillRect(210,190,40,110);
const tela = document.querySelector('canvas');
tela.width = 600;
tela.height = 400;

let pincel = tela.getContext('2d');
pincel.fillStyle = '#D3D3D3';
pincel.rect(0,0,600,400);
pincel.fill();

function  Circulo(x, y, raio){
    pincel.beginPath();
    pincel.fillStyle = 'blue';
    pincel.arc(x,y,raio,0, 2 * Math.PI);
    pincel.fill();
}
limparTela = () => pincel.clearRect(0,0,600,400);

let X = 20;
let Y = 50;
let sentido = 1;

let raio = 20;
let pulso = 1;

function atualizaTela(){
    limparTela();
    //if(X > 600){ sentido = -1} else if(X < 0){ sentido = 1};
    if(raio > 30) { pulso = -1} else if(raio < 20) { pulso = 1};
    Circulo(X, Y, raio);
    //X = X + sentido;
    raio = raio + pulso;
}
let teste = setInterval(atualizaTela, 10);

Limpar = () =>  clearInterval(teste);


const esquerda = 37;
const cima = 38;
const direita = 39;
const baixo = 40;
const taxa = 10;

function Teclado(event){
    if(event.keyCode == 39){
        X += taxa;
    }else if(event.keyCode == 37){
        X -= taxa;
    }else if(event.keyCode == 40){
        Y += taxa;
    }else if(event.keyCode == 38){
        Y -= taxa;
    }
}

document.onkeydown = Teclado;
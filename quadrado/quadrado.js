const tela = document.querySelector('canvas');

tela.width = 600;
tela.height = 600;

let pincel = tela.getContext('2d');

function quadrado(x, y, cor){
    pincel.fillStyle = cor;
    pincel.fillRect(x, y, 50, 50);

    pincel.strokeStyle = 'black';
    pincel.strokeRect(x,y,50,50);
    pincel.fill();
}

for(let pos = 0; pos < 600; pos += 50){
    quadrado(pos, 0, 'red');
}
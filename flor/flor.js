const tela = document.querySelector('canvas');
tela.width = 500;
tela.height = 500;
let pincel = tela.getContext('2d');

function flor(x, y){
    pincel.fillStyle = 'red';
    pincel.beginPath()
    pincel.arc(x, y, 50, 0, 2 * Math.PI);
    pincel.fill();

    pincel.fillStyle = 'black';
    pincel.beginPath()
    pincel.arc(x + 100, y, 50, 0, 2 * Math.PI);
    pincel.fill();

    pincel.fillStyle = 'orange';
    pincel.beginPath()
    pincel.arc(x - 100, y, 50, 0, 2 * Math.PI);
    pincel.fill();

    pincel.fillStyle = 'yellow';
    pincel.beginPath()
    pincel.arc(x, y - 100, 50, 0, 2 * Math.PI);
    pincel.fill();

    pincel.fillStyle = 'blue';
    pincel.beginPath()
    pincel.arc(x, y + 100, 50, 0, 2 * Math.PI);
    pincel.fill();
}
flor(200,200);
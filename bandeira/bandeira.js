const tela = document.querySelector('canvas');
tela.width = 600;
tela.height = 400;

let pincel = tela.getContext('2d');

pincel.fillStyle = 'green';
pincel.fillRect(0,0,600,400);

pincel.fillStyle = 'yellow';
pincel.beginPath();
pincel.moveTo(0,200);
pincel.lineTo(300,0);
pincel.lineTo(600,200);
pincel.fill();

pincel.beginPath();
pincel.moveTo(0,200);
pincel.lineTo(300,400);
pincel.lineTo(600,200);
pincel.fill();

pincel.fillStyle = 'blue';
pincel.beginPath();
pincel.arc(300,200,100,0,2 * Math.PI);
pincel.fill();